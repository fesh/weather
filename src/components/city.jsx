import React from 'react';
import { PropTypes } from 'prop-types';
import {Card, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import * as styles from './city.css';

const City = props => {
    const {
        location,
        temperature, 
        icon,
        wind
    } = props.weather;

    return (
        <Card className={styles.card}>
            <CardMedia>
                <div>
                    <img src={icon} className={styles.icon}/>
                </div>
            </CardMedia>
            <CardTitle title={location.title} subtitle={`${location.latitude}, ${location.longitude}`}/>
            <CardText>
                <h1>Now {temperature.current} &#8451;</h1>
                <h3 className={styles.feels}>Feels like {temperature.feelslike} &#8451;</h3>
                <h1>Wind {wind} km/h</h1>
            </CardText>
        </Card>
    );
};

City.propTypes = {
    weather: PropTypes.object
};

export default City;