import { all } from 'redux-saga/effects';
import { geoSaga } from './geo';
import { weatherSaga } from './weather';

function* watchAll() {
    yield all([
        geoSaga,
        weatherSaga
    ]);
}

export default watchAll;
