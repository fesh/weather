import { takeEvery, put } from 'redux-saga/effects';
import * as actions from '../constants/actions';

const moscow = {
    latitude: 55.7522200,
    longitude: 37.6155600,
    altitude: null,
    accuracy: 29,
    altitudeAccuracy: null,
    speed: 0
};

function getCurrentPosition() {
    return new Promise(resolve => {
        navigator.geolocation.getCurrentPosition(
            data => resolve(data.coords),
            () => resolve(moscow)
        );  
    });
}

function* askGeo() {
    const position = yield getCurrentPosition();
    yield put({ type: actions.ASK_GEO_SUCCEEDED, position});
    return position;
}

const geoSaga = takeEvery(actions.ASK_GEO_REQUESTED, askGeo);

export {
    geoSaga,
    askGeo
};