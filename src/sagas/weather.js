import { takeEvery, call, put } from 'redux-saga/effects';
import * as actions from '../constants/actions';
import { askGeo } from './geo';

function* askWeather() {
    const position = yield call(askGeo);

    try {
        const url = `https://api.wunderground.com/api/cdcb336cfe45d4c5/conditions/q/${position.latitude},${position.longitude}.json`;
        const weather = yield fetch(url).then(response => response.json()).then(data => data && data.current_observation);

        yield put({
            type: actions.ASK_WEATHER_SUCCEEDED,
            data: {
                location: {
                    title: weather.observation_location && weather.observation_location.full,
                    latitude: weather.observation_location && weather.observation_location.latitude,
                    longitude: weather.observation_location && weather.observation_location.longitude
                },
                temperature: {
                    current: weather.temp_c,
                    feelslike: weather.feelslike_c
                },
                icon: weather.icon_url,
                wind: weather.wind_kph
            }
        });
    } catch (error) {
        yield put({ type: actions.ASK_WEATHER_FAILED, error });
    }
}

const weatherSaga = takeEvery(actions.ASK_WEATHER_REQUESTED, askWeather);

export {
    weatherSaga,
    askWeather
};
