import React from 'react';
import { Route } from 'react-router'
import Layout from '../pages/layout';
import CityPage from '../pages/city';
import AboutPage from '../pages/about';

const routes = (
    <Route path="/" component={Layout} >
        <Route path="city" component={CityPage}/>
        <Route path="about" component={AboutPage}/>
    </Route>
);

export default routes;