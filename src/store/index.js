import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer as routing, routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import * as reducers from '../reducers';
import sagas from '../sagas';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    combineReducers({
        ...reducers,
        routing
    }),
    composeEnhancers(applyMiddleware(
        routerMiddleware(browserHistory), 
        sagaMiddleware
    ))
);

const history = syncHistoryWithStore(browserHistory, store);

sagaMiddleware.run(sagas);

export {
    store, 
    history
};
