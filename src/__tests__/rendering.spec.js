import React from 'react';
import renderer from 'react-test-renderer';
import City from '../components/city';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

test('Render > city', () => {
    const weather = {
        location: {
            title: 'moscow',
            latitude: 11,
            longitude: 22
        },
        temperature: {
            current: 33,
            feelslike: 44
        },
        icon: 'https://facebook.github.io/jest/img/jest-outline.svg',
        wind: 1.1
    };
    const tree = renderer.create( 
        <MuiThemeProvider>
            <City weather={weather}/>
        </MuiThemeProvider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});
