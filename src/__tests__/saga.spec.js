import { assert } from 'chai';
import { call } from 'redux-saga/effects';
import { askWeather as saga } from '../sagas/weather';
import { askGeo } from '../sagas/geo';

test('Saga', () => {
    let generator = saga();

    assert.deepEqual(
        generator.next().value,
        call(askGeo),
        'detect geo'
    );
});
