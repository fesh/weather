import { city as reducer } from '../reducers';
import * as actions from '../constants/actions';

test('City reducer > init', () => {
    let state = reducer(undefined, {});
    const initState = {
        weather: {
            location: {
                title: '',
                latitude: 0,
                longitude: 0
            },
            temperature: {
                current: 0,
                feelslike: 0
            },
            icon: '',
            wind: 0
        }
    };
    expect(state).toEqual(initState);
});


test('City reducer > weather', () => {
    let state = reducer(undefined, {
        type: actions.ASK_WEATHER_SUCCEEDED,
        data: {
            location: {
                title: 'moscow'
            }
        }
    });
    expect(state.weather.location.title).toEqual('moscow');
});
