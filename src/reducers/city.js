import * as actions from '../constants/actions';

const initState = {
    weather: {
        location: {
            title: '',
            latitude: 0,
            longitude: 0
        },
        temperature: {
            current: 0,
            feelslike: 0
        },
        icon: '',
        wind: 0
    }
};

const city = (state = initState, action) => {
    switch (action.type) {
        case actions.ASK_WEATHER_SUCCEEDED: 
            return {
                ...state,
                weather: action.data
            };
        default:
            return state;
    }
}

export default city;
