// @flow
declare var WeatherType: {
    location: {
        title: string,
        latitude: number,
        longitude: number
    },
    temperature: {
        current: number,
        feelslike: number
    },
    icon: string,
    wind: number
};

export {
    WeatherType
};