import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import City from '../components/city';
import * as actions from '../constants/actions';

class WeatherContainer extends React.Component {
    constructor() {
        super();
    }

    static get propTypes() { 
        return { 
            action: PropTypes.func,
            weather: PropTypes.object
        }; 
    }

    componentDidMount() {
        this.props.action(actions.ASK_WEATHER_REQUESTED);
    }

    render() {
        return (
            <div>
                <City weather={this.props.weather}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    weather: state.city.weather
});

const mapDispatchToProps = dispatch => ({
    action: (type, data) => dispatch({type, data})
});

export default connect(mapStateToProps, mapDispatchToProps)(WeatherContainer);