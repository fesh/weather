import React from 'react';

const AboutPage = () => {
    return (
        <div>
            <h2>About</h2>
            <p>
                Hi there. This is small weather application. The main goal is to combine all frontend technologies together.
            </p>
            <p>
                Used technologies:
            </p>
            <ul>
                <li>React</li>
                <li>Redux</li>
                <li>Saga</li>
                <li>Flow</li>
                <li>ESlint</li>
                <li>Webpack</li>
                <li>Browser geo api</li>
            </ul>
            <p>
                <a href="https://github.com/atomlipetsk/" target="_blank" rel="noopener noreferrer">My github profile</a>
            </p>
        </div>
    );
};

export default AboutPage;
