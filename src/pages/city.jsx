import React from 'react';
import WeatherContainer from '../containers/weather';

const CityPage = () => {
    return (
        <div>
            <h2>Weather in my city</h2>
            <WeatherContainer/>
        </div>
    );
};

export default CityPage;
