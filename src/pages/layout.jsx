import React from 'react';
import { Link } from 'react-router';
import { PropTypes } from 'prop-types';
import AppBar from 'material-ui/AppBar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import * as styles from './layout.css';

const Layout = props => (
    <MuiThemeProvider>
        <div>
            <AppBar title="Weather app">
                <div className={styles.appbarButtons}>
                    <FlatButton 
                        label="Show weather in my city"
                        containerElement={<Link to="/city" />}
                    />
                    <FlatButton 
                        label="About"
                        containerElement={<Link to="/about" />}
                    />
                </div>
            </AppBar>
            <div className={styles.content}>
                {props.children}
            </div>
        </div>
    </MuiThemeProvider>
);

Layout.propTypes = {
    children: PropTypes.any
};

export default Layout;