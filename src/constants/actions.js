export const ASK_GEO_SUCCEEDED = 'ASK_GEO_SUCCEEDED';
export const ASK_GEO_FAILED = 'ASK_GEO_FAILED';
export const ASK_GEO_REQUESTED = 'ASK_GEO_REQUESTED';

export const ASK_WEATHER_SUCCEEDED = 'ASK_WEATHER_SUCCEEDED';
export const ASK_WEATHER_FAILED = 'ASK_WEATHER_FAILED';
export const ASK_WEATHER_REQUESTED = 'ASK_WEATHER_REQUESTED';