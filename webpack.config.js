let path = require('path'),
    config = require('./package.json'),
    webpack = require('webpack'),
    flowtypePlugin = require('flowtype-loader/plugin');

module.exports = {
    entry: ['babel-polyfill', './' + config.main],
    output: {
        path: path.resolve(__dirname, 'public'),
        publicPath: '/assets/',
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            //enforce: 'pre',
            exclude: /(node_modules|bower_components|\.spec\.js)/,
            loaders: ['babel-loader', 'flowtype-loader', 'eslint-loader']
        }, {
            test: /\.css$/,
            loader: 'style-loader!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
        }]
    },
    devServer: {
        contentBase: 'public/',
        inline: true,
        historyApiFallback: true
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new flowtypePlugin(),
        new webpack.ProgressPlugin({ profile: false })
    ]
};
